﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KlasySterowanie
{

    public class Do
    {
        public Do()
        {}

        public DateTime Date { get; set; }
        public int Port { get; set; }
        public int Time { get; set; }

        public void AddWork(DateTime Date, int Port, int Time)
        {
            this.Date = Date;
            this.Port = Port;
            this.Time = Time;
        }

    }

    public class Raspberry
    {
        public Raspberry()
        {
            Todo = new List<Do>();
        }

        public int Version { get; set; }
        public List<Do> Todo { get; set; }

        public void AddToDo(Do Todo)
        {
            this.Todo.Add(Todo);
        }

        public void SetVersion(int Version)
        {
            this.Version = Version;
        }

        public void RemoveToDo(Do todo)
        {
            this.Todo.Remove(todo);
        }
    }
}