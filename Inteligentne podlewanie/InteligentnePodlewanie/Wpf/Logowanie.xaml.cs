﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Usluga;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for Logowanie.xaml
    /// </summary>
    public partial class Logowanie : Page
    {
        public Logowanie()
        {
            InitializeComponent();
        }

        private void Loguj_Click(object sender, RoutedEventArgs e)
        {
            Usluga.ClientWebServiceSoapClient service = new ClientWebServiceSoapClient();
            string temporarykey = service.Authorisation(LoginBox.Text, PasswordBox.Password);
            if (temporarykey != "-1")
            {
                Menu zalogowany = new Menu();
                zalogowany.PrzekazTemporaryKey(temporarykey);
                this.NavigationService.Navigate(zalogowany);
            }
            else
            {
                // Nie ma takiego bicia
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            this.NavigationService.RemoveBackEntry();
        }
    }
}
