﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Usluga;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for Harmonogram.xaml
    /// </summary>
    public partial class Harmonogram : Page
    {
        private String _temporaryKey;
        public Harmonogram(String temporaryKey)
        {
            InitializeComponent();
            _temporaryKey = temporaryKey;
        }

        private void PowrotButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Kalendarz.SelectedDate = DateTime.Now;
            Kalendarz_SelectedDatesChanged(sender,null);

            Usluga.ClientWebServiceSoapClient service = new ClientWebServiceSoapClient();
            var zadania = service.GetWork(_temporaryKey);

            foreach (var element in zadania.Todo)
            {
                if (Kalendarz.SelectedDates.Equals(element.Date))
                {

                }
                else
                {
                    Kalendarz.SelectedDates.Add(element.Date);
                }
            }
        }

        private void Kalendarz_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            ListaDnia.Items.Clear();
            Usluga.ClientWebServiceSoapClient service = new ClientWebServiceSoapClient();
            var zadania = service.GetWork(_temporaryKey);

            var Query = from query in zadania.Todo
                        where query.Date.Year == Kalendarz.SelectedDate.Value.Year 
                        && query.Date.Month == Kalendarz.SelectedDate.Value.Month
                        && query.Date.Day >= Kalendarz.SelectedDate.Value.Day
                        && query.Date.Day < (Kalendarz.SelectedDate.Value.Day + 1)
                        select query;
            testowe.Text = Kalendarz.SelectedDate.ToString();
            foreach (var element in Query)
            {
                ListViewItem a = new ListViewItem();
                a.Content = element.Date.ToShortTimeString() + "-> Port: " + element.Port + " Przez " + element.Time +
                            " minut";
                ListaDnia.Items.Add(a);
            }
        }
    }
}
