﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for DodawanieZadania.xaml
    /// </summary>
    public partial class DodawanieZadania : Page
    {
        private String _temporaryKey;

        public DodawanieZadania(String temporaryKey)
        {
            _temporaryKey = temporaryKey;
            
            InitializeComponent();
            for (int i = 0; i < 24; i++)
            {
                Godz.Items.Add(i);
            }
            for (int i = 0; i < 60; i++)
            {
                Min.Items.Add(i);
            }
            for (int i = 0; i < 60; i++)
            {
                Sek.Items.Add(i);
            }
            for (int i = 0; i < 8; i++)
            {
                co.Items.Add(i);
            }
            for (int i = 0; i < 60; i++)
            {
                jakDlugo.Items.Add(i);
            }
            date.SelectedDate = DateTime.Now;
            Godz.Text = DateTime.Now.Hour.ToString();
            Min.Text = DateTime.Now.Minute.ToString();
            Sek.Text = DateTime.Now.Second.ToString();
            co.Text = "0";
            jakDlugo.Text = "1";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime a = new DateTime(
                date.SelectedDate.Value.Year,
                date.SelectedDate.Value.Month,
                date.SelectedDate.Value.Day,
                int.Parse(Godz.SelectedValue.ToString()),
                int.Parse(Min.SelectedValue.ToString()),
                int.Parse(Sek.SelectedValue.ToString()));

            using (Usluga.ClientWebServiceSoapClient host = new Usluga.ClientWebServiceSoapClient())
            {
                host.AddWork(_temporaryKey, a, int.Parse(co.Text), int.Parse(jakDlugo.Text));
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }
    }
}
