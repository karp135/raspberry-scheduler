﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        private String _temporaryKey;

        public Menu()
        {
            InitializeComponent();
        }

        public void PrzekazTemporaryKey(String temporaryKey)
        {
            _temporaryKey = temporaryKey;
        }

        private void HarmonogramButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Harmonogram(_temporaryKey));
        }

        private void DodajZadanieButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new DodawanieZadania(_temporaryKey));
        }

        private void WylogujButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Logowanie());
        }

        private void ZmianahaslaButton_Click(object sender, RoutedEventArgs e)
        {
            ZmianaHasla zalogowany = new ZmianaHasla();
            zalogowany.PrzekazTemporaryKey(_temporaryKey);
            this.NavigationService.Navigate(zalogowany);
        }
    }
}
