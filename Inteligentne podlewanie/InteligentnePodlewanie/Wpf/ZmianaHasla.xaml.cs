﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Usluga;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for ZmianaHasla.xaml
    /// </summary>
    public partial class ZmianaHasla : Page
    {
        private String _temporaryKey;

        public ZmianaHasla()
        {
            InitializeComponent();
        }

        private void Zmien_Click(object sender, RoutedEventArgs e)
        {
            Usluga.ClientWebServiceSoapClient service = new ClientWebServiceSoapClient();
            
            if (NoweHasloBox.Password == NoweHasloPowtorzBox.Password)
            {
                service.ChangePass(_temporaryKey, NoweHasloBox.Password);
                this.NavigationService.Navigate(new Logowanie());
            }
            else
            {
                // Dopisać komunikat o różnych hasłach
            }
        }

        public void PrzekazTemporaryKey(String temporarykey)
        {
            _temporaryKey = temporarykey;
        }

        private void PowrotButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }
    }
}
