﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Email
{
    class SendMessage
    {
        MailMessage message = new MailMessage();
        SmtpClient smtp = new SmtpClient("smtp.gmail.com");
        
        public SendMessage(String to, String from, String subject, String message)
        {
            this.message.From = new MailAddress(from, from);
            this.message.To.Add(new MailAddress(to));
            this.message.Subject = subject;
            this.message.Body = message;

            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("adres", "haslo");
            smtp.EnableSsl = true;
            smtp.Port = 587;
        }

       public void Send()
       {
           smtp.Send(message);
       }
    }
}
