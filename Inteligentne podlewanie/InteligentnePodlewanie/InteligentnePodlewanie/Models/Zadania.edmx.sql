
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 05/05/2014 23:48:03
-- Generated from EDMX file: C:\Users\Paweł\documents\visual studio 2012\Projects\InteligentnePodlewanie\InteligentnePodlewanie\Models\Zadania.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Zadania.Model];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UzytkownikUrzadzenie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UrzadzenieSet] DROP CONSTRAINT [FK_UzytkownikUrzadzenie];
GO
IF OBJECT_ID(N'[dbo].[FK_UrzadzenieZadanie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ZadanieSet] DROP CONSTRAINT [FK_UrzadzenieZadanie];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[UzytkownikSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UzytkownikSet];
GO
IF OBJECT_ID(N'[dbo].[UrzadzenieSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UrzadzenieSet];
GO
IF OBJECT_ID(N'[dbo].[ZadanieSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZadanieSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UzytkownikSet'
CREATE TABLE [dbo].[UzytkownikSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(50)  NOT NULL,
    [Haslo] nvarchar(50)  NOT NULL,
    [KodTymczasowy] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UrzadzenieSet'
CREATE TABLE [dbo].[UrzadzenieSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UzytkownikId] int  NOT NULL,
    [Nazwa] nvarchar(100)  NOT NULL,
    [Kod] nvarchar(max)  NOT NULL,
    [Wersja] int  NOT NULL
);
GO

-- Creating table 'ZadanieSet'
CREATE TABLE [dbo].[ZadanieSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Port] int  NOT NULL,
    [DataCzas] datetime  NOT NULL,
    [CzasDzialania] int  NOT NULL,
    [UrzadzenieId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UzytkownikSet'
ALTER TABLE [dbo].[UzytkownikSet]
ADD CONSTRAINT [PK_UzytkownikSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UrzadzenieSet'
ALTER TABLE [dbo].[UrzadzenieSet]
ADD CONSTRAINT [PK_UrzadzenieSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ZadanieSet'
ALTER TABLE [dbo].[ZadanieSet]
ADD CONSTRAINT [PK_ZadanieSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UzytkownikId] in table 'UrzadzenieSet'
ALTER TABLE [dbo].[UrzadzenieSet]
ADD CONSTRAINT [FK_UzytkownikUrzadzenie]
    FOREIGN KEY ([UzytkownikId])
    REFERENCES [dbo].[UzytkownikSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownikUrzadzenie'
CREATE INDEX [IX_FK_UzytkownikUrzadzenie]
ON [dbo].[UrzadzenieSet]
    ([UzytkownikId]);
GO

-- Creating foreign key on [UrzadzenieId] in table 'ZadanieSet'
ALTER TABLE [dbo].[ZadanieSet]
ADD CONSTRAINT [FK_UrzadzenieZadanie]
    FOREIGN KEY ([UrzadzenieId])
    REFERENCES [dbo].[UrzadzenieSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UrzadzenieZadanie'
CREATE INDEX [IX_FK_UrzadzenieZadanie]
ON [dbo].[ZadanieSet]
    ([UrzadzenieId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------