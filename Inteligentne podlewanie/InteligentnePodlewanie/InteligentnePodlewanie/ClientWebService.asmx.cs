﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using InteligentnePodlewanie.Models;
using KlasySterowanie;
using Newtonsoft.Json;
using RestSharp;

namespace InteligentnePodlewanie
{
    /// <summary>
    /// Summary description for ClientWebService
    /// </summary>
    [WebService(Namespace = "http://localhost/ClientWebService.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ClientWebService : System.Web.Services.WebService
    {
        private Random _random = new Random(Environment.TickCount);
        
        // Jeżeli użytkownik poda odpowiedni klucz to true, inaczej false
        private bool isAuthorized(String temporaryKey)
        {
            ZadaniaContextDb db = new ZadaniaContextDb();

            var Query = from query in db.UzytkownikSet.ToList()
                        where query.KodTymczasowy == temporaryKey
                        select query;

            if (Query.Count() == 1)
            {
                return true;
            }
            return false;
        }
        
        // Generuje losową zmienną string o określonej długości
        public string RandomString(int length)
        {
            string chars = "0123456789abcdefghijklmnopqrstuvwxyz";
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[_random.Next(chars.Length)]);

            return builder.ToString();
        }

        // Autoryzacja za pomocą loginu i hasła. Zwraca losowy ciąg znaków o długości 20 i to jest kod, dzięki któremu użytkownik się autoryzuje
        [WebMethod]
        public String Authorisation(String login, String pass)
        {
            var db = new ZadaniaContextDb();

            var Query = from query in db.UzytkownikSet.ToList()
                        where query.Login == login && query.Haslo == pass
                        select query;
            try
            {
                Query.ToList()[0].KodTymczasowy = RandomString(20);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return "-1";
            }
            
            db.SaveChanges();
            return Query.ToList()[0].KodTymczasowy;
        }

        // Pobieranie zadań do wykonania po podaniu odpowiedniego klucza
        [WebMethod] 
        public Raspberry GetWork(String  temporaryKey)
        {
            if (isAuthorized(temporaryKey) == true)
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UzytkownikSet.ToList()
                            where query1.KodTymczasowy == temporaryKey
                            select query1;

                var Query2 = from query2 in db.UrzadzenieSet.ToList()
                            where query2.UzytkownikId == Query1.ToList()[0].Id
                            select query2;

                var Query3 = from query3 in db.ZadanieSet.ToList()
                             where query3.UrzadzenieId == Query2.ToList()[0].Id
                             select query3;

                Raspberry todo = new Raspberry();
                todo.SetVersion(Query2.ToList()[0].Wersja);

                for (int i = 0; i < Query3.Count(); i++)
                {
                    Do robic = new Do();
                    robic.AddWork(Query3.ToList()[i].DataCzas, Query3.ToList()[i].Port, Query3.ToList()[i].CzasDzialania);
                    todo.AddToDo(robic);
                }
                return todo;
            }
            return null;
        }

        // Dodawanie zadania do bazy. Wymagane jest podanie kodu temporaryKey i całej reszty parametrów. Zwraca true jeżeli wszystko poszło dobrze
        [WebMethod]
        public bool AddWork(String temporaryKey, DateTime dateTime, int port, int time)
        {
            if (isAuthorized(temporaryKey) == true)
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UzytkownikSet.ToList()
                             where query1.KodTymczasowy == temporaryKey
                             select query1;

                var Query2 = from query2 in db.UrzadzenieSet.ToList()
                             where query2.UzytkownikId == Query1.ToList()[0].Id
                             select query2;

                var Query3 = from query3 in db.ZadanieSet.ToList()
                             where query3.UrzadzenieId == Query2.ToList()[0].Id
                             select query3;
                
                // Tworze zadanie i wypelniam danymi
                Zadanie tmp = new Zadanie();
                tmp.DataCzas = dateTime;
                tmp.Port = port;
                tmp.CzasDzialania = time;
                tmp.UrzadzenieId = Query2.ToList()[0].Id;

                // zwiększam numer wersji o 1
                Query2.ToList()[0].Wersja = Query2.ToList()[0].Wersja + 1;

                // dodaje zadanie do listy
                //db.ZadanieSet.
                //Query3.ToList().Add(tmp);
                db.ZadanieSet.Add(tmp); // nie jestem pewien czy to działa poprawnie. Warto przetestowaćs
                //Query2.ToList()[0].Zadanie = Query3.ToList();
                //Query1.ToList()[0].Urzadzenie = Query2.ToList();
                

                // zapisuje zmiany
                db.SaveChanges();

                return true;
            }
            return false;
        }

        // Zmiana hasła użytkownika. Wymagane jest podanie kodu temporaryKey i nowego hasła. Zwraca true jeżeli wszystko poszło dobrze. Słabo zabezpieczone, można zmienić hasło nie znając starego!
        [WebMethod]
        public bool ChangePass(String temporaryKey, String newPass)
        {
            if (isAuthorized(temporaryKey) == true)
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UzytkownikSet.ToList()
                             where query1.KodTymczasowy == temporaryKey
                             select query1;

                Query1.ToList()[0].Haslo = newPass;

                // zapisuje zmiany
                db.SaveChanges();

                return true;
            }
            return false;
        }

        // Zmiana zadania z bazy. Wymagane jest podanie kodu temporaryKey, id zadania i całej reszty parametrów. Słabo zabezpieczone, można zmienić komuś zadanie!!
        [WebMethod]
        public bool ChangeWork(String temporaryKey, int workID, DateTime dateTime, int port, int time)
        {
            if (isAuthorized(temporaryKey) == true)
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UzytkownikSet.ToList()
                             where query1.KodTymczasowy == temporaryKey
                             select query1;

                var Query2 = from query2 in db.UrzadzenieSet.ToList()
                             where query2.UzytkownikId == Query1.ToList()[0].Id
                             select query2;

                var Query3 = from query3 in db.ZadanieSet.ToList()
                             where query3.UrzadzenieId == Query2.ToList()[0].Id && query3.Id == workID
                             select query3;

               
                Query3.ToList()[0].DataCzas = dateTime;
                Query3.ToList()[0].Port = port;
                Query3.ToList()[0].CzasDzialania = time;

                Query2.ToList()[0].Wersja = Query2.ToList()[0].Wersja + 1;

                // zapisuje zmiany
                db.SaveChanges();

                return true;
            }
            return false;
        }

        // Usunięcie zadania z bazy. Wymagane jest podanie kodu temporaryKey i id zadania. Słabo zabezpieczone, można usunąć komuś zadanie!!
        [WebMethod]
        public bool RemoveWork(String temporaryKey, int workID)
        {
            if (isAuthorized(temporaryKey) == true)
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UzytkownikSet.ToList()
                             where query1.KodTymczasowy == temporaryKey
                             select query1;

                var Query2 = from query2 in db.UrzadzenieSet.ToList()
                             where query2.UzytkownikId == Query1.ToList()[0].Id
                             select query2;

                var Query3 = from query3 in db.ZadanieSet.ToList()
                             where query3.UrzadzenieId == Query2.ToList()[0].Id && query3.Id == workID
                             select query3;

                db.ZadanieSet.Remove(Query3.ToList()[0]);

                Query2.ToList()[0].Wersja = Query2.ToList()[0].Wersja + 1;

                // zapisuje zmiany
                db.SaveChanges();

                return true;
            }
            return false;
        }

        // Pobranie aktualnej temperatury w Poznaniu
        [WebMethod]
        public double GetTemperature()
        {
            RestClient client = new RestClient("http://api.openweathermap.org/data/2.5/weather?q=");
            var request = new RestRequest("Poznan,pl", Method.GET);
            IRestResponse<RootObject> response2 = client.Execute<RootObject>(request);
            RootObject tmp = new RootObject();
            tmp = JsonConvert.DeserializeObject<RootObject>(response2.Content);
            return tmp.main.temp - 273.15;
        }

        // Pobranie aktualnej pogody w Poznaniu
        [WebMethod]
        public string GetWeather()
        {
            RestClient client = new RestClient("http://api.openweathermap.org/data/2.5/weather?q=");
            var request = new RestRequest("Poznan,pl", Method.GET);
            IRestResponse<RootObject> response2 = client.Execute<RootObject>(request);
            RootObject tmp = new RootObject();
            tmp = JsonConvert.DeserializeObject<RootObject>(response2.Content);
            return tmp.weather[0].main;
        }
    }

    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public int pressure { get; set; }
        public int humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public int deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class RootObject
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }
}
