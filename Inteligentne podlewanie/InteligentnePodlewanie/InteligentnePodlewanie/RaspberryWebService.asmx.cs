﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Services;
using InteligentnePodlewanie.Models;
using KlasySterowanie;

namespace InteligentnePodlewanie
{
    /// <summary>
    /// Summary description for RaspberryWebService
    /// </summary>
    [WebService(Namespace = "http://localhost/RaspberryWebService.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RaspberryWebService : System.Web.Services.WebService
    {
        // Jeżeli klucz jest poprawny to true, inaczej false
        private bool isAuthorized(String key)
        {
            ZadaniaContextDb db = new ZadaniaContextDb();
            var Query = from query in db.UrzadzenieSet.ToList()
                        where query.Kod == key
                        select query;

            if (Query.Count() == 1)
            {
                return true;
            }
            return false;
        }

        // Pobieranie zadań dla urządzenia od podanym kluczu
        [WebMethod]
        public Raspberry GetWork(String key)
        {
            ZadaniaContextDb db = new ZadaniaContextDb();
            if (isAuthorized(key))
            {
                var Query = from query in db.UrzadzenieSet.ToList()
                            where query.Kod == key
                            select query;

                var Query2 = from query2 in db.ZadanieSet.ToList()
                             where query2.UrzadzenieId == Query.ToList()[0].Id
                             select query2;

                Raspberry todo = new Raspberry();
                todo.SetVersion(Query.ToList()[0].Wersja);
                
                for (int i = 0; i < Query2.Count(); i++)
                {
                    Do robic = new Do();
                    robic.AddWork(Query2.ToList()[i].DataCzas, Query2.ToList()[i].Port, Query2.ToList()[i].CzasDzialania);
                    todo.AddToDo(robic);
                }
                return todo;
            }
            return null;
        }


        //[WebMethod]
        //public void AddWork(DateTime kiedy, int co, int jakDlugo)
        //{
        //    using (var db = new ZadaniaContextDb())
        //    {
        //        var nowy = new Uzytkownik();
        //        nowy.Login = "bpit2";
        //        nowy.Haslo = "1234";
        //        var urzadzenie = new Urzadzenie();
        //        urzadzenie.Kod = "bla";
        //        urzadzenie.Nazwa = "blabla";
        //        urzadzenie.Wersja = 1;
        //        var zadanie = new Zadanie(co, kiedy, jakDlugo);
        //        urzadzenie.Zadanie.Add(zadanie);
        //        nowy.Urzadzenie.Add(urzadzenie);
        //        db.UzytkownikSet.Add(nowy);
        //        db.SaveChanges();

        //    }

        //    version++;
        //    baza.SetVersion(version);
        //    Do a = new Do();
        //    a.AddWork(kiedy,co, jakDlugo);
        //    baza.AddToDo(a);
        //}

        //[WebMethod]
        //public Raspberry ToDo()
        //{
        //    UsunPrzestarzaleWpisy();
        //    return baza;
        //}

        //public static void UsunPrzestarzaleWpisy()
        //{
        //    for (int i = 0; i < baza.Todo.Count; i++)
        //    {
        //        if (baza.Todo[i].Date <= DateTime.Now)
        //        {
        //            baza.RemoveToDo(baza.Todo[i]);
        //        }
        //    }
        //}
    }
}
