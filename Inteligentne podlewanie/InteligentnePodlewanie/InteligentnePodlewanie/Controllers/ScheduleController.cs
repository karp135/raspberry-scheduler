﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KlasySterowanie;

namespace InteligentnePodlewanie.Controllers
{
    public class ScheduleController : ApiController
    {
        RaspberryWebService a = new RaspberryWebService();
        // GET api/schedule
        // pobieranie zadań z listy http://localhost/api/schedule?key=blabla
        public Raspberry Get(String key)
        {
            return a.GetWork(key);
        }


        // POST api/schedule
        public void Post([FromBody]string value)
        {
        }

        // PUT api/schedule/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/schedule/5
        public void Delete(int id)
        {
        }
    }
}
