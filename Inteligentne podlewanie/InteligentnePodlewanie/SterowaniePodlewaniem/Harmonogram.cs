﻿using System;
using System.Threading;
using KlasySterowanie;
using Newtonsoft.Json;
//using PiSharp.LibGpio.Entities;
using RestSharp;
//using PiSharp.LibGpio;

namespace SterowaniePodlewaniem
{
    public class Harmonogram
    {
        private Raspberry obiekt;
        private int time;
        private int version = 0;
        private String IP;
        Random liczba = new Random();

        public Harmonogram(String IP)
        {
            obiekt = new Raspberry();
            time = 0;
            this.IP = IP;
        }

        public void Run()
        {
			//LibGpio.Gpio.SetupChannel(BroadcomPinNumber.Eighteen, Direction.Output);
            Console.WriteLine(DateTime.Now);
            while (true)
            {
                if (time == 0)
                {
                    Pobieranie();
                }
                if ((time + liczba.Next(2)) % 7 == 0) Console.WriteLine(DateTime.Now);
                RobCoDoZrobienia();
                time = (time + 1)%10; // zmienić później na większą liczbę (zbyt duże obciążenie zapytaniami)
                Thread.Sleep(1000);
            }
        }

        public void Pobieranie()
        {
            RestClient client = new RestClient(IP);
            var request = new RestRequest("Schedule", Method.GET);
            IRestResponse<Raspberry> response2 = client.Execute<Raspberry>(request);
            Raspberry tmp = new Raspberry();
            tmp = JsonConvert.DeserializeObject<Raspberry>(response2.Content);
            if (tmp.Version != version)
            {
                obiekt = tmp;
                version = obiekt.Version;
            }
        }

        public void RobCoDoZrobienia()
        {
            for (int i = 0; i < obiekt.Todo.Count; i++ )
            {
                if (obiekt.Todo[i].Date <= DateTime.Now)
                {
                        Pracuj(obiekt.Todo[i]);
                        obiekt.RemoveToDo(obiekt.Todo[i]);
                }
            }
        }

        private void Pracuj(Do praca)
        {
            ThreadStart nowaPraca = delegate
                {
                    new Harmonogram(IP).Podlewaj(praca.Port, praca.Time);
                }; 
            new Thread(nowaPraca).Start();
        }

        private void Podlewaj(int port, int time)
        {
            Console.WriteLine(DateTime.Now + ": Zaczynam lać wodę na porcie " + port);
            if (port == 0)
            {
                //LibGpio.Gpio.OutputValue(BroadcomPinNumber.Eighteen, true);
				//LibGpio.Gpio.OutputValue(BroadcomPinNumber.Eighteen, false);
            }
            if (port == 1)
            {
                //LibGpio.Gpio.SetupChannel(BroadcomPinNumber.Four, Direction.Output);
                //LibGpio.Gpio.OutputValue(BroadcomPinNumber.Four, true);  Thread.Sleep(time * 1000);
                //LibGpio.Gpio.OutputValue(BroadcomPinNumber.Four, false);
            }
            if (port == 2)
            {
                //LibGpio.Gpio.SetupChannel(BroadcomPinNumber.Seventeen, Direction.Output);
                //LibGpio.Gpio.OutputValue(BroadcomPinNumber.Seventeen, true);
                //LibGpio.Gpio.OutputValue(BroadcomPinNumber.Seventeen, false);
            }
            Thread.Sleep(time * 60000);
            Console.WriteLine(DateTime.Now + ": Kończę lać wodę na porcie "+port);
        }
    }
}
                                                                           