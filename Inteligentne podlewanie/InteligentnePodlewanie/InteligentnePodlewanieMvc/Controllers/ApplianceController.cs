﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InteligentnePodlewanieMvc.Models;
using WebMatrix.WebData;
using InteligentnePodlewanieMvc.Filters;

namespace InteligentnePodlewanieMvc.Controllers
{
    public class ApplianceController : Controller
    {
        //
        // GET: /Appliance/

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [InitializeSimpleMembership]
        public ActionResult List()
        {
            var db = new ZadaniaContextDb();
            if (WebSecurity.IsCurrentUser("admin"))
            {
                return View(db.UrzadzenieSet.ToList());
            }
            else
            {
                var Query1 = from query1 in db.UrzadzenieSet.ToList()
                             where query1.UzytkownikId == WebSecurity.GetUserId(User.Identity.Name)
                             select query1;

                return View(Query1.ToList());
            }
        }

        //
        // GET: /Appliance/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Appliance/Create
        [Authorize(Roles = "AdminUser")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Appliance/Create
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        [InitializeSimpleMembership]
        public ActionResult Create(Urzadzenie appliance)
        {
            try
            {
                var db = new ZadaniaContextDb();
                appliance.Wersja = 1;
                db.UrzadzenieSet.Add(appliance);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Appliance/Edit/5
        [Authorize(Roles = "AdminUser")]
        //[InitializeSimpleMembership]
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Appliance/Edit/5

        [HttpPost]
        [Authorize(Roles = "AdminUser")]
        [InitializeSimpleMembership]
        public ActionResult Edit(int id, Urzadzenie model)
        {
            try
            {
                var db = new ZadaniaContextDb();

                Urzadzenie appliance = db.UrzadzenieSet.First(i => i.Id == id);
                appliance.Nazwa = model.Nazwa;
                appliance.Kod = model.Kod;
                appliance.Wersja++;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Appliance/Delete/5
        [Authorize(Roles = "AdminUser")]
        [InitializeSimpleMembership]
        public ActionResult Delete(int id)
        {
            var db = new ZadaniaContextDb();
            Urzadzenie appliance = db.UrzadzenieSet.First(i => i.Id == id);
            return View(appliance);
        }

        //
        // POST: /Appliance/Delete/5

        [HttpPost]
        [Authorize(Roles = "AdminUser")]
        [InitializeSimpleMembership]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var db = new ZadaniaContextDb();
                Urzadzenie appliance = db.UrzadzenieSet.First(i => i.Id == id);
                db.UrzadzenieSet.Remove(appliance);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }
    }
}
