﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InteligentnePodlewanieMvc.Models;
using InteligentnePodlewanieMvc.Filters;
using WebMatrix.WebData;

namespace InteligentnePodlewanieMvc.Controllers
{
    public class TaskController : Controller
    {
        //
        // GET: /Task/

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [InitializeSimpleMembership]
        public ActionResult List(int ApplianceId)
        {
            ViewData["ApplianceId"] = ApplianceId;
            var db = new ZadaniaContextDb();

            var Query1 = from query1 in db.UrzadzenieSet.ToList()
                         where query1.UzytkownikId == WebSecurity.GetUserId(User.Identity.Name)
                         select query1;

            bool is_authenticated = false;
            foreach (Urzadzenie row in Query1.ToList())
            {
                if (row.Id == ApplianceId)
                {
                    ViewBag.ApplianceName = row.Nazwa;
                    is_authenticated = true;
                    break;
                }
            }

            if (is_authenticated)
            {
                var Query3 = from query3 in db.ZadanieSet.ToList()
                             where query3.UrzadzenieId == ApplianceId
                             select query3;

                return View(Query3.ToList());
            }
            else
            {
                return View("List");
            }
        }

        //
        // GET: /Task/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Task/Create

        public ActionResult Create(int ApplianceId)
        {
            return View();
        }

        //
        // POST: /Task/Create

        [HttpPost]
        [Authorize]
        [InitializeSimpleMembership]
        public ActionResult Create(Zadanie task, int ApplianceId, string chosenDate, string chosenHour)
        {
            try
            {
                var db = new ZadaniaContextDb();

                var Query1 = from query1 in db.UrzadzenieSet.ToList()
                             where query1.UzytkownikId == WebSecurity.GetUserId(User.Identity.Name)
                             select query1;

                bool is_authenticated = false;
                foreach (Urzadzenie row in Query1.ToList())
                {
                    if (row.Id == ApplianceId)
                    {
                        is_authenticated = true;
                        break;
                    }
                }

                if (is_authenticated)
                {
                    chosenDate += " " + chosenHour;
                    task.DataCzas = DateTime.Parse(chosenDate);
                    task.UrzadzenieId = ApplianceId;
                    db.ZadanieSet.Add(task);
                    db.SaveChanges();                    
                }
                return RedirectToAction("List", new { ApplianceId = ApplianceId });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Task/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Task/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Task/Delete/5
        [Authorize]
        [InitializeSimpleMembership]
        public ActionResult Delete(int id)
        {
            var db = new ZadaniaContextDb();
            Zadanie task = db.ZadanieSet.First(i => i.Id == id);
            return View(task);            
        }

        //
        // POST: /Task/Delete/5

        [HttpPost]
        [Authorize]
        [InitializeSimpleMembership]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var db = new ZadaniaContextDb();
                Zadanie task = db.ZadanieSet.First(i => i.Id == id);
                db.ZadanieSet.Remove(task);
                db.SaveChanges();
                return RedirectToAction("List", new { ApplianceId = task.UrzadzenieId });
            }
            catch
            {
                return View();
            }
        }
    }
}
