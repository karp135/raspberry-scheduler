
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/01/2014 02:43:40
-- Generated from EDMX file: C:\Users\Praktykant\Source\Workspaces\Test\InteligentnePodlewanie\InteligentnePodlewanieMvc\Models\Zadania.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [aspnet-InteligentnePodlewanieMvc-20140528230923];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UzytkownikUrzadzenie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ApplianceSet] DROP CONSTRAINT [FK_UzytkownikUrzadzenie];
GO
IF OBJECT_ID(N'[dbo].[FK_UrzadzenieZadanie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TaskSet] DROP CONSTRAINT [FK_UrzadzenieZadanie];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO
IF OBJECT_ID(N'[dbo].[ApplianceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ApplianceSet];
GO
IF OBJECT_ID(N'[dbo].[TaskSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TaskSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NOT NULL,
    [TemporaryCode] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ApplianceSet'
CREATE TABLE [dbo].[ApplianceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Version] int  NOT NULL
);
GO

-- Creating table 'TaskSet'
CREATE TABLE [dbo].[TaskSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Port] int  NOT NULL,
    [StartDateTime] datetime  NOT NULL,
    [DurationTime] int  NOT NULL,
    [ApplianceId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ApplianceSet'
ALTER TABLE [dbo].[ApplianceSet]
ADD CONSTRAINT [PK_ApplianceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskSet'
ALTER TABLE [dbo].[TaskSet]
ADD CONSTRAINT [PK_TaskSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'ApplianceSet'
ALTER TABLE [dbo].[ApplianceSet]
ADD CONSTRAINT [FK_UzytkownikUrzadzenie]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownikUrzadzenie'
CREATE INDEX [IX_FK_UzytkownikUrzadzenie]
ON [dbo].[ApplianceSet]
    ([UserId]);
GO

-- Creating foreign key on [ApplianceId] in table 'TaskSet'
ALTER TABLE [dbo].[TaskSet]
ADD CONSTRAINT [FK_UrzadzenieZadanie]
    FOREIGN KEY ([ApplianceId])
    REFERENCES [dbo].[ApplianceSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UrzadzenieZadanie'
CREATE INDEX [IX_FK_UrzadzenieZadanie]
ON [dbo].[TaskSet]
    ([ApplianceId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------